<?php
/**
 * The Sidebar containing the main widget areas.
 * If no widgets are set, the content below will be displayed
 *
 * @package blm_basic
 */
?>
<aside id="secondary" class="primary-sidebar right-block" role="complementary">
	
	<?php if ( ! dynamic_sidebar( 'secondary' ) ) : ?>

		
	<?php endif; ?>
	
</aside><!-- #secondary -->