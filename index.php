<?php
/**
 * Master Template. This template will be used to display your blog posts and pages if page.php does not exist.
 *
 * @package blm_basic
 */

get_header(); ?>

<div class="banner">
		<img src="<?php echo get_template_directory_uri().'/images/banner-news.jpg'; ?>" alt="" srcset="<?php echo get_template_directory_uri().'/images/banner-news.jpg'; ?> 1400w, <?php echo get_template_directory_uri().'/images/banner-news-mobile.jpg'; ?> 768w" sizes="(max-width: 1452px) 100vw, 1452px">	
</div>

<section class="site-main row">
	<div class="container">

		<div id="primary" class="primary-content left-block">
			<main id="main" class="content-area" role="main">
		
			<?php if (have_posts()) : while (have_posts()) : the_post() ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<h2 class="post-title"><?php the_title(); ?></h2>
					
					<div class="entry-content">
						<?php the_excerpt(); ?>
					</div><!-- .entry-content -->
	
				</article><!-- #post-## -->

			<?php endwhile; endif; ?>
			
			<?php blm_basic_paging_nav(); ?>
		
			</main><!-- #main -->
		</div><!-- #primary -->

		<?php //get_sidebar(); ?>
		
	</div><!-- .container -->
</section><!-- .site-main -->
<?php get_footer(); ?>