<?php
/**
 * The template for displaying the front page.
 *
 * @package blm_basic
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="row">

	<div id="slider" class="slideshow">
	<?php if( get_field( 'carousel' ) ): ?>
		<div class="carousel">
		<?php while( has_sub_field( 'carousel' ) ): ?>
			
			<?php if(get_sub_field( 'slide_link' ) ) : ?>
				<a href="<?php echo the_sub_field( 'slide_link' ); ?>"><img src="<?php echo the_sub_field( 'slide_image' ); ?>" alt="<?php echo the_sub_field( 'slide_title' ); ?>" srcset="<?php echo the_sub_field( 'slide_image' ); ?> 1400w, <?php echo the_sub_field( 'slide_image_mobile' ); ?> 700w" sizes="(max-width: 1400px) 100vw, 1400px" /></a>
			<?php else : ?>
				<img src="<?php echo the_sub_field( 'slide_image' ); ?>" alt="<?php echo the_sub_field( 'slide_title' ); ?>" srcset="<?php echo the_sub_field( 'slide_image' ); ?> 1400w, <?php echo the_sub_field( 'slide_image_mobile' ); ?> 700w" sizes="(max-width: 1400px) 100vw, 1400px" />
			<?php endif; ?>

	  	<?php endwhile; ?>
		</div>
	<?php endif; ?>
	</div>

</div>

<main class="site-main row">
	<div class="container">

		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->

	</div><!-- .container -->
</main><!-- .site-main -->

<aside class="callout">
	<?php if( get_field( 'callout' ) ): ?>

	<?php while( has_sub_field( 'callout' ) ): ?>
		<div class="callout-item">
			<a href="<?php echo the_sub_field( 'link' ); ?>"><img src="<?php echo the_sub_field( 'image' ); ?>" alt=""></a>
		</div>
	 <?php endwhile; ?>

	<?php endif; ?>
</aside>

<?php endwhile; ?>

<?php get_footer(); ?>