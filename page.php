<?php
/**
 * Master Template. This template will be used to display your blog posts and pages if page.php does not exist.
 *
 * @package blm_basic
 */

get_header(); ?>

<div class="banner">
	<?php if( has_post_thumbnail() ) : ?>
		<?php the_post_thumbnail( 'full' ); ?>
	<?php else : ?>
		<img src="<?php echo get_template_directory_uri().'/images/banner-default.jpg'; ?>" alt="" srcset="<?php echo get_template_directory_uri().'/images/banner-default.jpg'; ?> 1400w, <?php echo get_template_directory_uri().'/images/banner-default-mobile.jpg'; ?> 768w" sizes="(max-width: 1452px) 100vw, 1452px">	
	<?php endif; ?>
</div>

<section class="site-main row">
	<div class="container">

		<div id="primary" class="primary-content left-block">
			<main id="main" class="content-area" role="main">
		
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

			<?php endwhile; ?>
		
			</main><!-- #main -->
		</div><!-- #primary -->

		<?php get_sidebar(); ?>
		
	</div><!-- .container -->
</section><!-- .site-main -->
<?php get_footer(); ?>