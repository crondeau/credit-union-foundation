<?php
/**
 * The header for our theme.
 *
  * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package blm_basic
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
	<![endif]-->
	
	<?php wp_head(); ?>
	
	<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/d1ac5d72-3188-4a39-82aa-a73ffe480a99.css"/>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">	
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'blm_basic' ); ?></a>

	<header id="masthead" class="site-header row" role="banner">	
		<div class="header-wrapper">	
		
			<div id="branding" class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
			</div>
		
			<nav id="site-navigation" class="site-navigation main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<p class="text">MENU</p>
					<span></span>
					<span></span>
					<span></span>
				</button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</nav>	

		</div>	
	</header>

	<div id="content" class="site-content">