<?php
/**
 * The Template for displaying all single posts.
 *
 * @package blm_basic
 */

get_header(); ?>

<div class="banner">
		<img src="<?php echo get_template_directory_uri().'/images/banner-news.jpg'; ?>" alt="" srcset="<?php echo get_template_directory_uri().'/images/banner-news.jpg'; ?> 1400w, <?php echo get_template_directory_uri().'/images/banner-news-mobile.jpg'; ?> 768w" sizes="(max-width: 1452px) 100vw, 1452px">	
</div>

<section class="site-main row">
	<div class="container">

		<div id="primary" class="primary-content left-block">
			<main id="main" class="content-area" role="main">
		
			<?php while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<h2 class="entry-title"><?php the_title(); ?></h2>
					<div class="entrymeta">
						<p><?php the_time('F j, Y'); ?></p>
					</div>
	
					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
	
					<nav class="navigation post-navigation" role="navigation">
						<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'blm_basic' ); ?></h1>
						<div class="nav-previous left-block"><?php previous_post_link( '%link' ); ?></div>
						<div class="nav-next right-block"><?php next_post_link( '%link' ); ?></div>
					</nav>
	
				</article><!-- #post-## -->

			<?php endwhile; ?>

			</main><!-- #main -->
		</div><!-- #primary -->

		<?php //get_sidebar(); ?>

	</div><!-- .container -->
</section><!-- .site-main -->
<?php get_footer(); ?>