jQuery(document).ready(function($){
	$('.carousel').slick({
		arrows: true,
		dots: true,
  	    fade: true,
	  	  responsive: [
	  	    {
	  	      breakpoint: 768,
	  	      settings: {
	  			  arrows: false,
	  	      }
	  	    }
	  	  ]
	});	
	
	//hide reveal on click
	$("#reveal div").hide();
	$("#reveal h3").click(function(){
		$(this).next("div").slideToggle()
		.siblings("div:visible").slideUp();
		$(this).toggleClass("active");
		$(this).siblings("h3").removeClass("active");
	});
});
