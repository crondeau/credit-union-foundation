<?php
/**
 * The template for displaying the footer.
 * 
 * @package blm_basic
 */
?>

	</div><!-- #content -->

	<footer id="footer" class="row site-footer" role="contentinfo">
		<div class="container">
			<p class="small-text">Copyright <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>. All rights reserved.<br>By accessing this website, you are agreeing to the <a href="https://cufoundation.org/terms-and-conditions/">terms of service</a>.</p>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>